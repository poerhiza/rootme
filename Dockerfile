FROM ubuntu:22.04

LABEL maintainer="Poe#6299"

ARG KERNEL_VERSION
ENV KERNEL_VERSION $KERNEL_VERSION

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y gcc make flex bison dkms dwarves linux-headers-${KERNEL_VERSION}

WORKDIR /src

CMD ["make"]
