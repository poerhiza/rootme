Either use the release ko file or build it:

```bash
# Update the docker-compose.yml with the kernel version you are running (a.k.a. - run `uname -r` and use that value)
docker-compose up
sudo insmod ./bin/<version>/rootme.ko
```

Trigger it on game day:

```bash
cp `which sleep` ./rootme
id
./rootme 1
id
```

Unload it:

```bash
sudo rmmod rootme
```
